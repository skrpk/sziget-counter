import React, { useState } from 'react';
import './App.css';
import moment from 'moment';

const SzigetTeam = () => {
  return (
    <div className="wrapper">
      <div className="top-level"><img src={require("./img/vetal.jpg") } /></div>
      <div className="mid-level">
        <img src={ require("./img/aganur.jpg") } />
        <Counter />
        <img src={ require("./img/anton.PNG") } />
      </div>
      <div className="low-level">
        <img src={ require("./img/jeka.jpg") } />
        <img src={ require("./img/vlad.jpg") } />
      </div>
    </div>
  );
}

const Counter = () => {
  const SzigetDate = moment('2019-08-07');
  const [months, setMonths] = useState(0);
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);

  const changeTime = () => {
    const timeLeft = moment(SzigetDate - moment());
    setMonths(timeLeft.month());
    setDays(timeLeft.date());
    setHours(timeLeft.hours());
    setMinutes(timeLeft.minutes());
    setSeconds(timeLeft.seconds());
  }

  setInterval(changeTime, 1000);

  return (
    <div className="App">
      <h2>!!!SZIGET BITCHES!!!</h2>
      <div className="timer">
        <h3>{ `${months} months ${days} days ${hours} hours ${minutes} minutes ${seconds} seconds left` }</h3>
      </div>
    </div>
  );
}

export default SzigetTeam;
